import React, {Component} from 'react';
import {
  View,
  Dimensions,
  StyleSheet,
  Modal,
  Keyboard,
  Text,
  Platform,
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native';

import {Textarea} from 'native-base';

import {AirbnbRating} from 'react-native-elements';

import EvaluateButton from './EvaluateButton';
import RatedPopUp from './RatedPopUp';

export default class Evaluation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sastifaction: 0,
      attendance: 0,
      observation: '',
      isModalRateVisible: false,
      buttonIsVisible: false,
      commentIsNecessary: false,
    };
  }

  changeModalRateVisibility = bool => this.setState({isModalRateVisible: bool});

  onPressRate() {
    this.changeModalRateVisibility(true);
    this.setAttendance();
    //console.log('çodal', this.isModalVisible)
  }

  componentDidMount() {
    const evaluation_satisfaction_status_id = this.props.data
      .evaluation_satisfaction_status_id;
    const evaluation_attendance_status_id = this.props.data
      .evaluation_attendance_status_id;
    const evaluation_observation = this.props.data.evaluation_description;

    this.setState({
      sastifaction:
        1 + [31, 30, 29, 28, 27].indexOf(evaluation_satisfaction_status_id),
      attendance: 1 + [34, 33, 32].indexOf(evaluation_attendance_status_id),
      observation: evaluation_observation,
    });

    if (this.props.buttonIsVisible) {
      this.setState({
        buttonIsVisible: true,
        sastifaction: 0,
        attendance: 0,
        observation: '',
      });
    } else {
      this.setState({
        buttonIsVisible: this.props.data.status_id == 21 ? true : false,
      });
    }
  }

  setSatifaction(text) {
    const array = [31, 30, 29, 28, 27];
    const sastifaction = array[text - 1];

    console.log(sastifaction);

    this.setState({
      sastifaction: sastifaction,
    });

    console.log(this.state);
  }

  setAttendance(text) {
    const array = [34, 33, 32];
    const attendance = array[text - 1];

    console.log(attendance);

    this.setState({
      attendance: attendance,
    });
    console.log(this.state);
  }

  handleUnhandledTouches() {
    Keyboard.dismiss();
    return false;
  }

  render() {
    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;
    return (
      <View
        style={styles.Box}
        onStartShouldSetResponder={this.handleUnhandledTouches}>
        <Text style={styles.Title}>Avaliação</Text>
        <View style={styles.Container}>
          <Text style={styles.Text}>Grau de Satisfação</Text>
          <AirbnbRating
            isDisabled={!this.state.buttonIsVisible}
            count={5}
            defaultRating={this.state.sastifaction}
            reviews={['Péssimo', 'Ruim', 'Regular', 'Boa', 'Ótima']}
            onFinishRating={this.setSatifaction.bind(this)}
            size={20}
          />
          <Text style={[styles.Text, {marginTop: 15}]}>
            Grau de Atendimento
          </Text>
          <AirbnbRating
            isDisabled={!this.state.buttonIsVisible}
            count={3}
            reviews={['Não Atendeu', 'Parcialmente', 'Totalmente']}
            defaultRating={this.state.attendance}
            size={20}
            onFinishRating={this.setAttendance.bind(this)}
          />

          <Modal
            transparent={true}
            visible={this.state.isModalRateVisible}
            onRequestClose={() => this.changeModalRateVisibility(false)}
            animationType="fade">
            <RatedPopUp
              changeModalRateVisibility={this.changeModalRateVisibility}
            />
          </Modal>

          <Textarea
            disabled={!this.state.buttonIsVisible}
            style={styles.Input}
            value={this.state.observation}
            onChangeText={observation => this.setState({observation})}
            placeholder="Críticas e sugestões"
            placeholderTextColor="#999"
            bordered
          />
          <EvaluateButton
            onClose={this.props.onClose}
            navigation={this.props.navigation}
            data={this.props.data}
            observation={this.state.observation}
            sastifaction={this.state.sastifaction}
            attendance={this.state.attendance}
            buttonIsVisible={this.state.buttonIsVisible}
            judgeType={this.props.judgeType}
          />
        </View>
      </View>
    );
  }
}

const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  Container: {
    marginLeft: 27,
    marginRight: 27,
    marginTop: 20,
    marginBottom: 20,
  },

  Box: {
    borderStyle: 'solid',
    borderColor: '#e4e4e4',
    borderWidth: 2,
    height: 'auto',
  },

  Title: {
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#e4e4e4',
    marginLeft: 20,
    marginRight: 20,
  },

  Observation: {
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 20,
    marginRight: 20,
  },

  Button: {
    width: '100%',
    height: 54,
    backgroundColor: '#3c8dbc',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Icon: {
    position: 'absolute',
    left: 20,
    color: '#202020',
    fontSize: 24,
  },

  TextLight: {
    fontSize: 14,
    color: '#FFF',
    fontWeight: '600',
    textAlign: 'center',
  },

  Text: {
    fontSize: 16,
  },

  Input: {
    width: '100%',
    height: 100,
    borderColor: '#EEE',
    borderWidth: 2,
    borderRadius: 4,
    marginTop: 20,
  },
});

// <Card
//   title="Avaliação"
//   onStartShouldSetResponder={this.handleUnhandledTouches}>
//   <Text>Grau de Satisfação</Text>
//   <AirbnbRating
//     isDisabled={!this.state.buttonIsVisible}
//     count={5}
//     defaultRating={this.state.sastifaction}
//     reviews={['Péssimo', 'Ruim', 'Regular', 'Boa', 'Ótima']}
//     onFinishRating={this.setSatifaction.bind(this)}
//     size={20}
//   />

//   <Text>Grau de Atendimento</Text>
//   <AirbnbRating
//     isDisabled={!this.state.buttonIsVisible}
//     count={3}
//     reviews={['Não Atendeu', 'Parcialmente', 'Totalmente']}
//     defaultRating={this.state.attendance}
//     size={20}
//     onFinishRating={this.setAttendance.bind(this)}
//   />
//   <Modal
//     transparent={true}
//     visible={this.state.isModalRateVisible}
//     onRequestClose={() => this.changeModalRateVisibility(false)}
//     animationType="fade">
//     <RatedPopUp
//       changeModalRateVisibility={this.changeModalRateVisibility}
//     />
//   </Modal>

//   <Textarea
//     disabled={!this.state.buttonIsVisible}
//     style={searchStyles.Input}
//     value={this.state.observation}
//     onChangeText={observation => this.setState({observation})}
//     placeholder="Críticas e sugestões"
//     placeholderTextColor="#999"
//     bordered
//   />

//   <EvaluateButton
//     onClose={this.props.onClose}
//     navigation={this.props.navigation}
//     data={this.props.data}
//     observation={this.state.observation}
//     sastifaction={this.state.sastifaction}
//     attendance={this.state.attendance}
//     buttonIsVisible={this.state.buttonIsVisible}
//     judgeType={this.props.judgeType}
//   />
// </Card>
