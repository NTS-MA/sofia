/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Animated,
  Dimensions,
  Keyboard,
  PanResponder,
  Platform,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

var modalHeight = 500;
const height = Dimensions.get('window').height;

export default class BannerCmpt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      container: new Animated.Value(height),
      opacity: new Animated.Value(0),
      modal: new Animated.Value(height),
    };
  }

  setHeight = event => {
    modalHeight = event.nativeEvent.layout.height;
  };

  handleOpen = () => {
    Animated.sequence([
      Animated.timing(this.state.container, {toValue: 0, duration: 100}),
      Animated.timing(this.state.opacity, {toValue: 1, duration: modalHeight}),
      Animated.spring(this.state.modal, {
        toValue: 0,
        bounciness: 5,
        useNativeDriver: true,
      }),
    ]).start();
    Keyboard.dismiss();
  };

  handleClose = () => {
    Animated.sequence([
      Animated.timing(this.state.modal, {
        toValue: modalHeight + height,
        duration: 250,
        useNativeDriver: true,
      }),
      Animated.timing(this.state.opacity, {toValue: 0, duration: modalHeight}),
      Animated.timing(this.state.container, {toValue: height, duration: 100}),
    ]).start();
  };

  render() {
    const translate = {
      transform: [{translateY: this.state.modal}],
    };

    const shadow = {
      opacity: this.state.opacity,
      transform: [{translateY: this.state.container}],
    };

    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

    return (
      <Animated.View style={[modal.Shadow, shadow]}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Animated.View style={[modal.Modal, translate]}>
            <View style={{position: 'absolute', right: -10, top: 10}}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.handleClose();
                }}>
                <View style={modal.CloseButton}>
                  <Icon name="close" style={modal.IconCloseButton} />
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View
              style={modal.Container}
              onLayout={event => {
                this.setHeight(event);
              }}>
              {this.props.content}
            </View>
          </Animated.View>
        </View>
      </Animated.View>
    );
  }
}

const modal = StyleSheet.create({
  Shadow: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    position: 'absolute',
    zIndex: 0,
  },

  Modal: {
    width: '90%',
    height: 'auto',
    backgroundColor: '#FFF',
    borderTopLeftRadius: 10,
    borderTopEndRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    zIndex: 1,
  },

  Container: {
    height: 'auto',
    marginLeft: 37,
    marginRight: 37,
    justifyContent: 'center',
    alignItems: 'center',
  },

  Text: {
    color: '#FFF',
    fontSize: 18,
  },

  CloseButton: {
    width: 80,
    height: 80,
  },

  IconCloseButton: {
    color: '#3c8dbc',
    alignSelf: 'center',
    fontSize: 26,
  },
});
