import PushNotification from 'react-native-push-notification';
import FixTimeOut from '../services/FixTimeOut';

PushNotification.configure({
  onNotification: function() {
    console.log('onRegister');
  },
  popInitialNotification: true,
  requestPermissions: true,
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
});

exports.localNotif = (id, message) => {
  PushNotification.localNotification({
    color: '#092272', // (optional) default: system default
    message: `A solicitação ${id} ${message}`, // (required)
    ignoreInForeground: false,
  });
};
