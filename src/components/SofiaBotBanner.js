/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-const-assign */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Image, Linking, StyleSheet, View, Text} from 'react-native';

import ButtonCmpt from './ButtonCmpt';
import BannerCmpt from './BannerCmpt';
import Styles from '../Styles/Styles';
import imgSofiaBot from '../resources/sofiabot.jpg';

export default class SofiaBotBanner extends Component {
  componentDidMount() {
    this.banner.handleOpen();
  }

  render() {
    return (
      <BannerCmpt
        content={
          <View
            style={{
              height: 'auto',
              paddingTop: 40,
              alignItems: 'center',
            }}>
            <Image style={styles.ModalImg} source={imgSofiaBot} />
            <Text style={styles.modalText}>
              O Núcleo de Telessaúde do HUUFMA lançou um novo serviço para
              ajudar na luta contra o novo coronavírus. A assistente virtual
              Sofia avalia a suspeita de COVID-19 e fornece orientações. O
              serviço está disponível para a população em geral em todo o
              Brasil, clique no botão abaixo para acessar!
            </Text>
            <View>
              <ButtonCmpt
                content={<Text style={Styles.TextLight}>Acessar!</Text>}
                color={'#3c8dbc'}
                handleFunction={() => {
                  Linking.openURL('http://sofia.huufma.br/chatbot');
                }}
              />
            </View>
          </View>
        }
        ref={banner => {
          this.banner = banner;
        }}
        {...this.props}
      />
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
  },

  modalView: {
    width: '90%',
    height: 400,
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  ModalBg: {
    width: '100%',
    height: '100%',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  ModalImg: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginBottom: 20,
  },

  modalText: {
    marginBottom: 25,
    textAlign: 'center',
  },

  CloseButton: {
    width: 50,
    height: 50,
  },

  IconCloseButton: {
    color: '#3c8dbc',
    alignSelf: 'center',
    fontSize: 26,
  },
});
