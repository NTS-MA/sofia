import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Platform,
  TouchableNativeFeedback,
  TouchableOpacity,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import Evaluation from '../services/Evaluation';

export default class EvaluateButton extends Component {
  judgeNormal(token, sastifaction, attendance, observation) {
    const solicitation_id = this.props.data.solicitation_id;
    Evaluation.judgeRequestMadeByMe(
      token,
      sastifaction,
      attendance,
      observation,
      solicitation_id,
    )
      .then(response => {
        console.log('Avaliação de questão respondida');
        console.debug(response);

        shouldUpdate = true;
        this.props.navigation.navigate('EvaluationFeedback', {shouldUpdate});
      })
      .catch(error => {
        console.log(err);
      });
  }

  judgeRelatedIssue(token, sastifaction, attendance, observation) {
    Evaluation.judgeRequest(
      token,
      sastifaction,
      attendance,
      observation,
      this.props.data.answer_id,
    )
      .then(response => {
        console.log('Avaliação de questão relacionada');
        console.debug(response);

        shouldUpdate = true;
        this.props.navigation.navigate('EvaluationFeedback', {shouldUpdate});
      })
      .catch(error => {
        console.log(error);
      });
  }

  async judge() {
    const sastifaction = this.props.sastifaction;
    const attendance = this.props.attendance;
    const observation = this.props.observation;

    const token = await AsyncStorage.getItem('token');

    if (this.props.judgeType == '1') {
      this.judgeNormal(token, sastifaction, attendance, observation);
    } else {
      this.judgeRelatedIssue(token, sastifaction, attendance, observation);
      this.props.onClose();
    }
  }

  render() {
    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;
    if (this.props.buttonIsVisible) {
      return (
        <View style={{width: '100%', marginTop: 20}}>
          <TouchablePlatformSpecific
            onPress={() => {
              this.judge();
            }}>
            <View style={styles.Button}>
              <Text style={styles.TextLight}>Avaliar</Text>
            </View>
          </TouchablePlatformSpecific>
        </View>
      );
    } else {
      return <Text />;
    }
  }
}

// <Button
// block
// success
// style={{marginLeft: 30, marginRight: 30, marginTop: 30}}
// onPress={() => {
//   this.judge();
// }}>
// <Text>Avaliar</Text>
// </Button>

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    marginLeft: 27,
    marginRight: 27,
    marginTop: 20,
  },

  Box: {
    borderStyle: 'solid',
    borderColor: '#e4e4e4',
    borderWidth: 2,
    height: 'auto',
  },

  Title: {
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#e4e4e4',
    marginLeft: 20,
    marginRight: 20,
  },

  Observation: {
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 20,
    marginRight: 20,
  },

  Button: {
    width: '100%',
    height: 54,
    backgroundColor: '#3c8dbc',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Icon: {
    position: 'absolute',
    right: 20,
    color: '#202020',
    fontSize: 24,
  },

  TextLight: {
    fontSize: 14,
    color: '#FFF',
    fontWeight: '600',
    textAlign: 'center',
  },

  Text: {
    fontSize: 16,
  },
});
