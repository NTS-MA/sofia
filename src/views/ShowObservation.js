/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Platform,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class ShowObservation extends Component {
  render() {
    const item = this.props.navigation.state.params.item;
    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;
    return (
      <View style={styles.Container}>
        <View style={styles.Box}>
          <Text style={styles.Title}>Observação</Text>
          <Text style={styles.Observation}>
            {
              this.props.navigation.state.params.item
                .solicitation_observations_description
            }
          </Text>
        </View>

        {item.status_id !== 20 && (
          <View style={{width: '100%', marginTop: 20}}>
            <TouchablePlatformSpecific
              onPress={() =>
                this.props.navigation.navigate('EditQuestion', {
                  item,
                  isReturnedRequest: true,
                })
              }>
              <View style={styles.Button}>
                <Icon name="edit" style={[styles.Icon, {color: '#FFF'}]} />
                <Text style={styles.TextLight}>Editar pergunta</Text>
              </View>
            </TouchablePlatformSpecific>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    marginLeft: 27,
    marginRight: 27,
    marginTop: 20,
  },

  Box: {
    borderStyle: 'solid',
    borderColor: '#e4e4e4',
    borderWidth: 2,
    height: 250,
  },

  Title: {
    fontSize: 16,
    textAlign: 'center',
    paddingTop: 15,
    paddingBottom: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#e4e4e4',
    marginLeft: 20,
    marginRight: 20,
  },

  Observation: {
    paddingTop: 15,
    paddingBottom: 15,
    marginLeft: 20,
    marginRight: 20,
  },

  Button: {
    width: '100%',
    height: 54,
    backgroundColor: '#3c8dbc',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Icon: {
    position: 'absolute',
    left: 20,
    color: '#202020',
    fontSize: 24,
  },

  TextLight: {
    fontSize: 14,
    color: '#FFF',
    fontWeight: '600',
    textAlign: 'center',
  },
});
