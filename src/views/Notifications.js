/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Dimensions,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import AsyncStorage from '@react-native-community/async-storage';
import {readNotif} from '../services/Request';

export default class Notifications extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasNotif: false,
    };
  }
  componentDidMount() {
    const {notifications} = this.props.navigation.state.params;
    if (notifications.length > 0) {
      this.setState({hasNotif: true});
    }
  }

  render() {
    const {notifications} = this.props.navigation.state.params;

    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

    return (
      <View>
        {this.state.hasNotif ? (
          <FlatList
            data={notifications}
            keyExtractor={item => item.notification_id.toString()}
            renderItem={({item}) => (
              <TouchablePlatformSpecific
                onPress={() =>
                  this.props.navigation.navigate('Overlay', {item})
                }>
                <View style={styles.Item}>
                  <View style={styles.Icon}>
                    <Icon name="mail-outline" size={30} color="#3c8dbc" />
                  </View>
                  <View style={styles.ContainerText}>
                    <Text style={styles.Text}>
                      A solicitação{' '}
                      <Text style={{color: '#3c8dbc'}}>
                        {item.solicitation_id}
                      </Text>{' '}
                      está com o status {item.status}
                    </Text>
                  </View>
                </View>
              </TouchablePlatformSpecific>
            )}
          />
        ) : (
          <View
            style={{
              height: Dimensions.get('window').height - 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="mail-outline" size={150} color="#888" />
            <Text style={{color: '#888', fontWeight: 'bold', marginTop: 10}}>
              Você não tem notificações no momento!
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Item: {
    width: '100%',
    height: 'auto',
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#fff',
  },

  Icon: {
    width: '15%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  ContainerText: {
    width: '80%',
    height: 'auto',
    justifyContent: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.5)',
    paddingTop: 15,
    paddingBottom: 15,
    marginRight: 10,
  },

  Text: {
    fontSize: 14,
    color: '#202020',
    fontWeight: 'normal',
  },

  Arrow: {
    width: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0, 0, 0, 0.5)',
  },
});
