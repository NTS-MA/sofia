/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  StatusBar,
  TextInput,
  Text,
  View,
} from 'react-native';

import ButtonCmpt from '../components/ButtonCmpt';
import ModalCmpt from '../components/ModalCmpt';
import styles from '../Styles/Styles';

import {recoverPassword} from '../services/Request';

export default class RecoverPassword extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      message: '',
      disabled: false,
    };
  }

  onRecoverPassword = async () => {
    this.setState({disabled: true});
    const {email} = this.state;

    recoverPassword(email)
      .then(response => {
        console.log(response);
        var message = response.data;

        this.setState({
          message: message,
          disabled: false,
        });

        this.modal.handleOpen();
      })
      .catch(error => {
        console.log(error);
        console.error(error);
      });
  };

  render() {
    return (
      <View>
        <StatusBar backgroundColor="#3c8dbc" barStyle="light-content" />
        <KeyboardAvoidingView
          behavior="padding"
          style={{
            height: Dimensions.get('window').height - 64,
            zIndex: 0,
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
          }}>
          <ScrollView>
            <Text style={([styles.TextDark], {textAlign: 'left'})}>
              Informe seu Email ou CPF para continuar.
            </Text>

            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="default"
              returnKeyType="next"
              style={styles.Input}
              value={this.state.email}
              onChangeText={text => {
                this.setState({
                  email: text,
                });
              }}
              ref={ref => (this.emailField = ref)}
            />

            <View style={{width: '100%', marginTop: 20}}>
              <ButtonCmpt
                disabled={this.state.disabled}
                handleFunction={this.onRecoverPassword.bind(this)}
                color={'#3c8dbc'}
                content={<Text style={styles.TextLight}>Entrar</Text>}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
        <ModalCmpt
          content={
            <View style={{marginBottom: 20}}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontWeight: '600',
                  marginBottom: 10,
                }}>
                {this.state.message}
              </Text>
            </View>
          }
          ref={modal => {
            this.modal = modal;
          }}
          {...this.props}
        />
      </View>
    );
  }
}
