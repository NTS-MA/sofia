/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Image,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  StatusBar,
  TouchableOpacity,
  TouchableNativeFeedback,
  Text,
  View,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

import login from '../services/Solicitant';

import styles from '../Styles/Styles';

import EmailInput from '../components/EmailInput';
import PasswordInput from '../components/PasswordInput';
import ModalComponent from '../components/ModalComponent';
import ButtonCmpt from '../components/ButtonCmpt';
import ModalCmpt from '../components/ModalCmpt';

import {AppEventsLogger} from 'react-native-fbsdk';

import logo from '../resources/SOFIA.png';

const height = Dimensions.get('window').height;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLoggedIn: 'false',
      token: '',
      isModalVisible: false,
      disabled: false,
    };
  }

  /*Remove header padrão*/
  static navigationOptions = {
    header: null,
  };

  /*Aciona a rotina de análise de login.
    Se tudo certo, redireciona o solicitante para a página inicial,
    senão, mostra uma mensagem de erro.*/
  onLoginButtonPress = async () => {
    this.setState({disabled: true});
    const email = this.state.email;
    const password = this.state.password;

    login(email, password)
      .then(response => {
        const {id, token, forwards_indications} = response;

        console.log(id);

        this.saveCredentials(id, token, forwards_indications);
        console.log(response);
      })
      .catch(response => {
        // this.handleOpen();
        this.modal.handleOpen();
        this.setState({disabled: false});
      });
  };

  /*Guarda o token de acesso no armazenamento local.*/
  saveCredentials = async (id, token, forwards_indications) => {
    try {
      await AsyncStorage.setItem('id', id.toString());
      await AsyncStorage.setItem('token', token);
      await AsyncStorage.setItem('logging', 'true');
      await AsyncStorage.setItem(
        'forwards_indications',
        forwards_indications.toString(),
      );

      console.log('ENCAMINHAMENTO ------------');
      console.log(await AsyncStorage.getItem('forwards_indications'));
      this.props.navigation.navigate('HomeScreen');
    } catch (error) {
      console.debug(error);
    }
  };

  componentDidMount() {
    if (Platform.OS === 'ios') {
      AppEventsLogger.logEvent('iOS');
    } else {
      AppEventsLogger.logEvent('Android');
    }
  }

  /*Abre o modal*/
  handleOpen = value => {
    this.setState({modalIsVisible: true, value: value});
  };

  /*Fecha o modal*/
  handleClose = () => {
    this.setState({modalIsVisible: false, disabled: false});
  };

  render() {
    const {modalIsVisible} = this.state;

    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

    return (
      <View>
        <StatusBar backgroundColor="#FFF" barStyle="dark-content" />

        <KeyboardAvoidingView
          behavior="padding"
          style={{
            marginLeft: 37,
            marginRight: 37,
            justifyContent: 'center',
            height: Dimensions.get('window').height,
            zIndex: -1,
          }}>
          <Image style={styles.loginLogo} source={logo} />

          <EmailInput props={this} />

          <PasswordInput props={this} />

          <View style={{width: '100%', marginTop: 20}}>
            <ButtonCmpt
              disabled={this.state.disabled}
              handleFunction={this.onLoginButtonPress.bind(this)}
              color={'#3c8dbc'}
              content={<Text style={styles.TextLight}>Entrar</Text>}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <View style={{width: '48%'}}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('SignUp');
                }}>
                <Text style={{color: '#2c6689', textAlign: 'center'}}>
                  Cadastrar-se
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{width: '48%'}}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('RecoverPassword');
                }}>
                <Text style={{color: '#2c6689', textAlign: 'center'}}>
                  Esqueci a senha
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>

        <ModalCmpt
          content={
            <View style={{marginBottom: 20}}>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 16,
                  fontWeight: '600',
                  marginBottom: 10,
                }}>
                E-mail ou senha incorretos!
              </Text>
              <Text
                style={{textAlign: 'center', fontSize: 14, color: '#4c4c4c'}}>
                Os dados inseridos não constam no nosso banco de dados. Tente
                novamente.
              </Text>
            </View>
          }
          ref={modal => {
            this.modal = modal;
          }}
          {...this.props}
        />
      </View>
    );
  }
}
