import React, {Component} from 'react';

import {
  BackHandler,
  Linking,
  Platform,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';

export default class Configurations extends Component {
  logout = async () => {
    await AsyncStorage.setItem('token', '');
    await AsyncStorage.setItem('logging', 'false');

    if (Platform.OS === 'ios') {
      this.props.navigation.navigate('Login');
    } else {
      BackHandler.exitApp();
    }
  };

  async checkShow() {
    if (this.state.checked) {
      await AsyncStorage.setItem('modalChecked', 'true');
    } else {
      await AsyncStorage.setItem('modalChecked', 'false');
    }
  }

  render() {
    const TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;
    return (
      <View style={styles.Container}>
        <View>
          <TouchablePlatformSpecific onPress={() => this.logout()}>
            <View style={styles.Item}>
              <Icon name="exit-to-app" style={styles.Icon} />
              <Text style={styles.Text}>Sair da conta</Text>
            </View>
          </TouchablePlatformSpecific>
          <TouchablePlatformSpecific
            onPress={() => {
              Linking.openURL('http://sofia.huufma.br/chatbot');
            }}>
            <View style={styles.Item}>
              <Icon name="public" style={styles.Icon} />
              <Text style={styles.Text}>Acesar Sofia chatbot</Text>
            </View>
          </TouchablePlatformSpecific>
        </View>
        <View style={styles.Version}>
          {Platform.OS === 'ios' ? (
            <Text>Versão do app: 2.4</Text>
          ) : (
            <Text>Versão do app: 7.0.3</Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    marginLeft: 27,
    marginRight: 27,
    justifyContent: 'space-between',
  },

  Item: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    marginTop: 10,
    paddingTop: 10,
    marginBottom: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },

  Icon: {
    fontSize: 30,
    color: '#7f7f7f',
    marginRight: 15,
  },

  Text: {
    fontSize: 20,
    color: '#3b3b3b',
  },

  Version: {
    alignItems: 'center',
    marginBottom: 50,
  },
});
