/* eslint-disable react-native/no-inline-styles */
/*RelatedIssueView.js*/
import React, {Component} from 'react';
import {
  ActivityIndicator,
  BackHandler,
  Modal,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableOpacity,
  View,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import Evaluation from '../components/Evaluation';
import AsyncStorage from '@react-native-community/async-storage';
import {getRequest} from '../services/Request';

export default class RelatedIssueView extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      answer: '',
      data: null,
      status_description: '',
      complement: '',
      attributes: '',
      permanent_education: '',
      references: '',
      sastifaction: 0,
      attendance: 0,
      showME: true,
    };
  }

  handleOpen = () => {
    this.setState({isVisible: true});
  };

  handleClose = () => {
    this.setState({isVisible: false});
    this.props.navigation.goBack();
  };

  componentDidMount() {
    this.getRelatedIssue();
  }

  componentWillMount = () => {
    const {isVisible} = this.state;
    BackHandler.addEventListener('hardwareBackPress', function() {
      if (!isVisible) {
        this.handleOpen();
        return true;
      }
      return false;
    });
  };

  /*Obtendo as questões enviadas para a Sofia pelo Token*/
  async getRelatedIssue() {
    const token = await AsyncStorage.getItem('token');
    const request_id = this.props.navigation.state.params.item.id;

    getRequest(token, request_id)
      .then(response => {
        this.setState({
          answer_id: response.data.answer_id,
          data: response.data,
          status_description: response.data.status_description,
          answer: response.data.answer,
          complement: response.data.complement,
          attributes: response.data.attributes,
          permanent_education: response.data.permanent_education,
          references: response.data.references,
          showME: false,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const {isVisible} = this.state;

    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

    return (
      <View>
        <View style={styles.HeaderBackground}>
          <TouchablePlatformSpecific onPress={this.handleOpen}>
            <View style={styles.HeaderBackButton}>
              <Icon name="arrow-back" style={{color: '#FFF', fontSize: 26}} />
            </View>
          </TouchablePlatformSpecific>
          <Text style={styles.HeaderTitle}>Pergunta relacionada</Text>
        </View>

        {this.state.showME ? (
          <View style={styles.load}>
            <ActivityIndicator size="large" color="#3c8dbc" />
          </View>
        ) : (
          <ScrollView>
            <View style={styles.Container}>
              <Text style={styles.Header}>
                {this.props.navigation.state.params.item.description}
              </Text>
              <View style={styles.Section}>
                <Text style={styles.Title}>Resposta</Text>
                <Text style={styles.Text}>{this.state.answer}</Text>
              </View>
              <View style={styles.Section}>
                <Text style={styles.Title}>Complemento</Text>
                <Text style={styles.Text}>{this.state.complement}</Text>
              </View>
              <View style={styles.Section}>
                <Text style={styles.Title}>Atributos</Text>
                <Text style={styles.Text}>{this.state.attributes}</Text>
              </View>
              <View style={styles.Section}>
                <Text style={styles.Title}>Educação Permanente</Text>
                <Text style={styles.Text}>
                  {this.state.permanent_education}
                </Text>
              </View>
              <View style={styles.Section}>
                <Text style={styles.Title}>Referências</Text>
                <Text style={styles.Text}>{this.state.references}</Text>
              </View>
              <Evaluation
                onClose={this.handleClose}
                navigation={this.props.navigation}
                data={this.state.data}
                judgeType="0"
                buttonIsVisible={true}
              />
              <TouchablePlatformSpecific
                onPress={() => this.props.navigation.goBack()}>
                <View style={styles.Button}>
                  <Text>Voltar para respostas encontradas</Text>
                </View>
              </TouchablePlatformSpecific>
            </View>
          </ScrollView>
        )}

        {isVisible && (
          <Modal animationType="slide" transparent={false} visible={isVisible}>
            <View style={styles.ModalContainer}>
              <Evaluation
                onClose={this.handleClose}
                navigation={this.props.navigation}
                data={this.state.data}
                judgeType="0"
                buttonIsVisible={true}
              />
              <TouchablePlatformSpecific
                onPress={() => this.props.navigation.goBack()}>
                <View style={styles.Button}>
                  <Text>Voltar para respostas encontradas</Text>
                </View>
              </TouchablePlatformSpecific>
            </View>
          </Modal>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  HeaderBackground: {
    width: '100%',
    height: 64,
    backgroundColor: '#3c8dbc',
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
    elevation: 2,
  },

  HeaderBackButton: {
    width: 54,
    height: 54,
    alignItems: 'center',
    justifyContent: 'center',
  },

  HeaderTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: '#fff',
    marginHorizontal: 16,
    textAlign: 'left',
  },

  ModalContainer: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 20,
    marginRight: 20,
  },

  Button: {
    width: '100%',
    height: 54,
    backgroundColor: '#eee',
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },

  Load: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  Container: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 74,
  },

  Section: {
    padding: 5,
    marginBottom: 10,
  },

  Header: {
    fontSize: 25,
    fontWeight: '600',
    padding: 5,
    paddingBottom: 15,
  },

  Title: {
    fontSize: 18,
    fontWeight: '500',
    paddingBottom: 5,
    marginBottom: 5,
    borderBottomColor: '#bbb',
    borderBottomWidth: 0.5,
  },
});
