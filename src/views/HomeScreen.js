import React, {Component} from 'react';
import {
  Alert,
  Dimensions,
  BackHandler,
  Image,
  Platform,
  RefreshControl,
  ScrollView,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  TouchableOpacity,
  View,
  StatusBarIOS,
} from 'react-native';

import {useHeaderHeight} from 'react-navigation-stack';

import Icon from 'react-native-vector-icons/MaterialIcons';

import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Pusher from 'pusher-js/react-native';

import NumberOfIssuesBadge from '../components/NumberOfIssuesBadge';
import ErrorNoInternetMessage from '../components/ErrorNoInternetMessage';
import {configureNotif, localNotif} from '../components/NotifService';
import SofiaBotBanner from '../components/SofiaBotBanner';

import PushNotification from 'react-native-push-notification';

import Requests, {getNotif} from '../services/Request';

import homeStyles from '../Styles/HomeScreen';
import styles from '../Styles/Styles';

var pusher;

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[stylesBar.statusBar, {backgroundColor}]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isConnected: false,
      refreshing: false,
      answeredIssues: [],
      submittedIssues: [],
      canceledIssues: [],
      draftIssues: [],
      notifs: [],
      existsRequestsWithoutEvaluation: false,
      hasNotif: false,
      icon: 'notifications-none',
      login: true,
      notif: false,
    };

    this.handleNotification = this.handleNotification.bind(this);
  }

  /*Remove header padrão*/
  static navigationOptions = {
    header: null,
  };

  /*Aciona a ação de sair do app.*/
  logout = async () => {
    await AsyncStorage.setItem('token', '');
    await AsyncStorage.setItem('logging', 'false');
    this.setState({notif: false});
    pusher.diconnect();

    if (Platform.OS === 'ios') {
      this.props.navigation.navigate('Login');
    } else {
      BackHandler.exitApp();
    }
  };

  /*Atualiza tela.*/
  refreshScreen = () => {
    this.setState({refreshing: true});
    this.load();
    this.sendOfflineRequests();
    this.setState({refreshing: false});
  };

  handleNotification(notification) {
    const notifications = this.state.notifs;
    this.props.navigation.navigate('Notifications', {
      notifications,
    });
  }

  /*Carregando questões enviadas, respondidas, canceladas e rascunhos*/
  async componentDidMount() {
    this.load();
    this.defineNotif();
  }

  async defineNotif() {
    const login = await AsyncStorage.getItem('logging');
    const token = await AsyncStorage.getItem('token');
    const user_id = await AsyncStorage.getItem('id');

    if (this.state.notif === false) {
      var that = this;
      PushNotification.configure({
        onNotification: function(notification) {
          that.handleNotification(notification);
        },
        popInitialNotification: true,
        requestPermissions: true,
        permissions: {
          alert: true,
          badge: true,
          sound: true,
        },
      });

      pusher = new Pusher('8e0be3d3fb4ea49f8cc4', {
        cluster: 'us2',
        authEndpoint: 'http://sofia.huufma.br/api/notification/auth',
        forceTLS: true,
        auth: {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      });

      var channel = pusher.subscribe('private-receivedanswer.' + user_id);
      channel.bind('App\\Events\\ReceivedAnswer', function(data) {
        localNotif(data.message.solicitation_id, data.message.status);
      });

      this.setState({notif: true});
    }
  }

  /*Carregando informações do app.*/
  load = () => {
    NetInfo.fetch().then(state => {
      this.setState({
        isConnected: state.isConnected,
      });

      this.loadAnsweredRequests();
      this.loadSentRequests();
      this.loadCanceledRequests();
      this.loadDraftRequests();
      this.loadNotif();
    });
  };

  /*Carregando as questões respondidas para a Sofia.*/
  loadAnsweredRequests = async () => {
    const token = await AsyncStorage.getItem('token');

    Requests.getAnsweredRequests(token)
      .then(response => {
        const answeredRequests = response.data;

        this.setState({
          answeredIssues: answeredRequests,
        });

        /*Procura por solicitações que não foram avaliadas.*/
        this.searchRequestsWithoutEvaluation();
      })
      .catch(error => {
        console.log(error);
      });
  };

  /*Carregando as questões enviadas para a Sofia.*/
  loadSentRequests = async () => {
    const token = await AsyncStorage.getItem('token');

    Requests.getSentRequests(token)
      .then(response => {
        const sentRequests = response.data;
        this.setState({submittedIssues: sentRequests});
      })
      .catch(error => {
        console.log(error);
      });
  };

  /*Obtendo as questões canceladas para a Sofia pelo Token*/
  loadCanceledRequests = async () => {
    const token = await AsyncStorage.getItem('token');

    Requests.getCanceledRequests(token)
      .then(response => {
        const canceledRequests = response.data;
        var novo = [];

        for (var index in canceledRequests) {
          var id = canceledRequests[index].id;
          if (novo.length === 0 || !novo.map(el => el.id).includes(id)) {
            var idx = canceledRequests.map(el => el.id).lastIndexOf(id);
            novo.push(canceledRequests[idx]);
          }
        }

        this.setState({
          canceledIssues: novo,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  /*Carregando as solicitações de rascunhos.*/
  loadDraftRequests = async () => {
    const token = await AsyncStorage.getItem('token');

    Requests.getDraftRequests(token)
      .then(response => {
        const draftRequests = response.data;
        this.setState({draftIssues: draftRequests});
      })
      .catch(error => {
        console.log(error);
      });
  };

  loadNotif = async () => {
    const user_id = await AsyncStorage.getItem('id');
    const token = await AsyncStorage.getItem('token');

    getNotif(user_id, token)
      .then(response => {
        const notifications = response.data.original;
        this.setState({notifs: notifications});

        if (this.state.notifs.length > 0) {
          this.setState({
            hasNotif: true,
            icon: 'notifications',
          });
        } else {
          this.setState({
            hasNotif: false,
            icon: 'notifications-none',
          });
        }
      })
      .catch(response => {
        console.log(response);
      });
  };

  /*Envia solicitações não enviadas por falta de internet. */
  sendOfflineRequests = async () => {
    var token = await AsyncStorage.getItem('token');
    var draftQuestions = await AsyncStorage.getItem('draftQuestions');

    draftQuestions = JSON.parse(draftQuestions);

    NetInfo.fetch().then(state => {
      if (state.isConnected && draftQuestions != null) {
        new Promise((resolve, reject) => {
          for (var index in draftQuestions) {
            const descripton = draftQuestions[index].description;
            const file_ids = draftQuestions[index].file_ids;

            Requests.sendRequest(token, descripton, file_ids)
              .then(response => {
                resolve(response);
              })
              .catch(response => {
                reject(response);
              });
          }
        })
          .then(response => {
            this.emptyOfflineRequests();
          })
          .catch(response => {
            console.log(response);
          });
      }
    });
  };

  /*Esvazia listas de rascunhos enviados offline*/
  emptyOfflineRequests = async () => {
    await AsyncStorage.setItem('draftQuestions', JSON.stringify([]));
  };

  /*Procura por solicitações que não foram avaliadas.*/
  searchRequestsWithoutEvaluation = () => {
    /*Configura como se houvesse solicitações sem avaliação inicialmente.*/
    this.setState({
      existsRequestsWithoutEvaluation: false,
    });

    var requestsWithoutEvaluation = [];

    new Promise((resolve, reject) => {
      try {
        for (var index in this.state.answeredIssues) {
          if (this.state.answeredIssues[index].status_id == 21) {
            requestsWithoutEvaluation.push(this.state.answeredIssues[index]);
            this.state.answeredIssues.splice(index, 1);
          }
        }
        resolve('OK');
      } catch (error) {
        reject(error);
      }
    })
      .then(response => {
        if (!requestsWithoutEvaluation) {
          this.setState({
            existsRequestsWithoutEvaluation: true,
          });
        }

        this.setState({
          answeredIssues: requestsWithoutEvaluation.concat(
            this.state.answeredIssues,
          ),
        });
      })
      .catch(error => {
        this.setState({
          answeredIssues: requestsWithoutEvaluation.concat(
            this.state.answeredIssues,
          ),
        });
      });
  };

  updateScreen() {
    try {
      if (this.props.navigation.state.params.shouldUpdate) {
        if (this.props.navigation.state.params.shouldUpdate == true) {
          this.load();

          this.props.navigation.state.params.shouldUpdate = false;
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    this.updateScreen();

    const answeredIssues = this.state.answeredIssues;
    const submittedIssues = this.state.submittedIssues;
    const canceledIssues = this.state.canceledIssues;
    const draftIssues = this.state.draftIssues;
    const estado = this.state;
    const notifications = this.state.notifs;
    const notif = pusher;

    let TouchablePlatformSpecific =
      Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;

    return (
      <View>
        {Platform.OS === 'ios' ? (
          <MyStatusBar backgroundColor="#3c8dbc" barStyle="light-content" />
        ) : (
          <StatusBar backgroundColor="#3c8dbc" barStyle="light-content" />
        )}

        <SafeAreaView>
          <View style={homeStyles.Header}>
            <TouchablePlatformSpecific
              onPress={() => {
                this.props.navigation.navigate('Notifications', {
                  notifications,
                });
              }}>
              <View style={homeStyles.NotifIconContainer}>
                <Icon name={this.state.icon} style={homeStyles.Icon} />
                {this.state.hasNotif && (
                  <View style={homeStyles.Badge}>
                    <Text style={homeStyles.Text}>
                      {this.state.notifs.length}
                    </Text>
                  </View>
                )}
              </View>
            </TouchablePlatformSpecific>

            <View style={homeStyles.TitleContainer}>
              <Image
                style={homeStyles.Image}
                source={require('../resources/ICONE.png')}
              />
              <Text style={homeStyles.Title}>Sofia</Text>
            </View>

            <TouchablePlatformSpecific
              onPress={() => {
                this.props.navigation.navigate('Configurations');
              }}>
              <View style={homeStyles.ConfigIconContainer}>
                <View>
                  <Icon name="settings" style={homeStyles.Icon} />
                </View>
              </View>
            </TouchablePlatformSpecific>
          </View>

          <View style={homeStyles.Body}>
            <ErrorNoInternetMessage isConnected={this.state.isConnected} />

            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.refreshScreen}
                />
              }>
              <View style={homeStyles.Container}>
                <TouchablePlatformSpecific
                  onPress={() => {
                    this.props.navigation.navigate('Search');
                  }}>
                  <View style={styles.Button}>
                    <Icon
                      name="question-answer"
                      style={[styles.Icon, {color: '#FFF'}]}
                    />
                    <Text style={styles.TextLight}>Como posso te ajudar?</Text>
                  </View>
                </TouchablePlatformSpecific>

                <TouchablePlatformSpecific
                  disabled={!this.state.isConnected}
                  onPress={() => {
                    this.props.navigation.navigate('AnsweredIssues', {
                      answeredIssues,
                      estado,
                    });
                  }}>
                  <View style={[styles.Button, homeStyles.Gray]}>
                    <Icon name="chat" style={styles.Icon} />
                    <Text style={styles.TextDark}>Respondidas</Text>
                    <View style={styles.Badge}>
                      <NumberOfIssuesBadge
                        number={this.state.answeredIssues.length}
                        isConnected={this.state.isConnected}
                        existsRequestsWithoutEvaluation={
                          this.state.existsRequestsWithoutEvaluation
                        }
                      />
                    </View>
                  </View>
                </TouchablePlatformSpecific>

                <TouchablePlatformSpecific
                  disabled={!this.state.isConnected}
                  onPress={() => {
                    this.props.navigation.navigate('SubmittedIssues', {
                      submittedIssues,
                    });
                  }}>
                  <View style={[styles.Button, homeStyles.Gray]}>
                    <Icon name="launch" style={styles.Icon} />
                    <Text style={styles.TextDark}>Enviadas</Text>
                    <View style={styles.Badge}>
                      <NumberOfIssuesBadge
                        number={this.state.submittedIssues.length}
                        isConnected={this.state.isConnected}
                      />
                    </View>
                  </View>
                </TouchablePlatformSpecific>

                <TouchablePlatformSpecific
                  disabled={!this.state.isConnected}
                  onPress={() => {
                    this.props.navigation.navigate('CanceledIssues', {
                      canceledIssues,
                    });
                  }}>
                  <View style={[styles.Button, homeStyles.Gray]}>
                    <Icon name="cancel" style={styles.Icon} />
                    <Text style={styles.TextDark}>Devolvidas</Text>
                    <View style={styles.Badge}>
                      <NumberOfIssuesBadge
                        number={this.state.canceledIssues.length}
                        isConnected={this.state.isConnected}
                      />
                    </View>
                  </View>
                </TouchablePlatformSpecific>

                <TouchablePlatformSpecific
                  disabled={!this.state.isConnected}
                  onPress={() => {
                    this.props.navigation.navigate('DraftIssues', {
                      draftIssues,
                    });
                  }}>
                  <View style={[styles.Button, homeStyles.Gray]}>
                    <Icon name="create" style={styles.Icon} />
                    <Text style={styles.TextDark}>Rascunhos</Text>
                    <View style={styles.Badge}>
                      <NumberOfIssuesBadge
                        number={this.state.draftIssues.length}
                        isConnected={this.state.isConnected}
                      />
                    </View>
                  </View>
                </TouchablePlatformSpecific>

                <TouchablePlatformSpecific
                  onPress={() => {
                    this.props.navigation.navigate('FAQ');
                  }}>
                  <View style={[styles.Button, homeStyles.Gray]}>
                    <Text
                      style={[styles.Icon, {marginLeft: 4, fontWeight: '700'}]}>
                      ?
                    </Text>
                    <Text style={styles.TextDark}>Dúvidas gerais</Text>
                  </View>
                </TouchablePlatformSpecific>
              </View>
            </ScrollView>
          </View>

          <SofiaBotBanner />
        </SafeAreaView>
      </View>
    );
  }
}

const X_WIDTH = 375;
const X_HEIGHT = 812;
const XSMAX_WIDTH = 414;
const XSMAX_HEIGHT = 896;

const {height, width} = Dimensions.get('window');

export const isIPhoneX = () =>
  Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS
    ? (width === X_WIDTH && height === X_HEIGHT) ||
      (width === XSMAX_WIDTH && height === XSMAX_HEIGHT)
    : false;

const STATUSBAR_HEIGHT = isIPhoneX() ? 44 : 20;

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const stylesBar = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#79B45D',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#33373B',
  },
});
