import PushNotification from 'react-native-push-notification';
import FixTimeOut from './FixTimeOut';

PushNotification.configure({
  onNotification: function(notification) {
    handleNotification(notification);
  },
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  popInitialNotification: true,
  requestPermissions: true,
});

export const handleNotification = notification => {
  const notifications = this.state.notifs;
  this.props.navigation.navigate('Notifications', {
    notifications,
  });
};

export const LocalNotification = (id, message) => {
  PushNotification.localNotification({
    color: '#092272',
    message: `A solicitação ${id} ${message}`,
  });
};
