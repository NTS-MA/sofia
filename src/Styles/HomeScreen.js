import {Dimensions, StyleSheet} from 'react-native';

const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    justifyContent: 'space-around',
    marginLeft: 37,
    marginRight: 37,
    margin: '1%',
    marginTop: '5%',
    marginBottom: '5%',
  },

  Header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: '10%',
    backgroundColor: '#3c8dbc',
  },

  Body: {
    width: '100%',
    height: '90%',
  },

  Gray: {
    backgroundColor: '#eee',
  },

  NotifIconContainer: {
    padding: 15,
    marginLeft: 5,
  },

  ConfigIconContainer: {
    padding: 15,
    marginRight: 5,
  },

  Icon: {
    alignSelf: 'center',
    color: '#FFF',
    fontSize: 26,
  },

  Badge: {
    position: 'absolute',
    top: 5,
    right: 5,
    backgroundColor: '#FFF',
    borderRadius: 20,
    textAlign: 'center',
  },

  Text: {
    color: '#3c8dbc',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
    marginLeft: 5,
    marginRight: 5,
    marginTop: 2,
    marginBottom: 2,
  },

  TitleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  Image: {
    width: 26,
    height: 29.5,
    marginRight: 10,
  },

  Title: {
    fontSize: 24,
    fontWeight: '600',
    color: '#FFF',
    textAlign: 'center',
  },
});

export default styles;
