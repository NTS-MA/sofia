import {AppRegistry} from 'react-native';
import App from './App';
import NotificationTaskName from './NotificationTask';

AppRegistry.registerComponent('app', () => App);
// AppRegistry.registerHeadlessTask(
//   'NotificationTask',
//   () => NotificationTaskName,
// );
