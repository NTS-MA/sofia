import React, {Component} from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';

import {createStackNavigator, createAppContainer} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

import Login from './src/Views/Login';
import SignUp from './src/Views/SignUp';
import HomeScreen from './src/Views/HomeScreen';
import SubmittedIssues from './src/Views/SubmittedIssues';
import DraftIssues from './src/Views/DraftIssues';
import CanceledIssues from './src/Views/CanceledIssues';
import AnsweredIssues from './src/Views/AnsweredIssues';
import Overlay from './src/Views/Overlay';
import Question from './src/Views/Question';
import EditQuestion from './src/Views/EditQuestion';
import ShowObservation from './src/Views/ShowObservation';
import ShowDetails from './src/Views/ShowDetails';
import RelatedQuestionsView from './src/Views/RelatedQuestionsView';
import Search from './src/Views/Search';
import SearchNoResults from './src/Views/SearchNoResults';
import RelatedIssueView from './src/Views/RelatedIssueView';
import FAQ from './src/Views/FAQ';
import FaqElement from './src/Views/FaqElement';
import Success from './src/Views/Success';
import EvaluationFeedback from './src/Views/EvaluationFeedback';
import ForwardQuestion from './src/Views/ForwardQuestion';
import RecoverPassword from './src/Views/RecoverPassword';
import Notifications from './src/Views/Notifications';
import Configurations from './src/Views/Configurations';

class Home extends Component {
  state = {
    logging: 'false',
  };

  componentDidMount() {
    this.retrieveData();
  }

  static navigationOptions = {
    headerStyle: {
      backgroundColor: 'white',
      elevation: null,
    },
    header: null,
  };

  async retrieveData() {
    const value = await AsyncStorage.getItem('logging');
    const email = await AsyncStorage.getItem('email');

    console.debug(value);

    this.setState({
      logging: value,
    });
  }

  render() {
    if (this.state.logging == 'true') {
      return (
        <View style={styles.container}>
          <StatusBar barStyle="light-content" backgroundColor="#3c8dbc" />
          <HomeScreen navigation={this.props.navigation} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" backgroundColor="#3c8dbc" />
        <Login navigation={this.props.navigation} />
      </View>
    );
  }
}

const SideTransition = (index, position, width) => {
  const sceneRange = [index - 1, index];
  // const outputOpacity = [0, 1];
  const outputWidth = [width, 0];
  const transition = position.interpolate({
    inputRange: sceneRange,
    outputRange: outputWidth,
  });

  return {
    // opacity: transition
    transform: [{translateX: transition}],
  };
};

const NavigationConfig = () => {
  return {
    screenInterpolator: (sceneProps) => {
      const {position} = sceneProps;
      const {scene} = sceneProps;
      const {index} = scene;
      const height = sceneProps.layout.initHeight;
      const width = sceneProps.layout.initWidth;

      return SideTransition(index, position, width);
    },
  };
};

const App = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        title: 'Home',
      },
    },

    HomeScreen: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'HomeScreen',
      },
    },

    EvaluationFeedback: {
      screen: EvaluationFeedback,
      navigationOptions: {
        title: 'EvaluationFeedback',
      },
    },

    RelatedQuestionsView: {
      screen: RelatedQuestionsView,
      navigationOptions: {
        title: 'Perguntas relacionadas',
      },
    },

    Search: {
      screen: Search,
      navigationOptions: {
        title: 'Como posso te ajudar?',
      },
    },

    SearchNoResults: {
      screen: SearchNoResults,
      navigationOptions: {
        title: 'Search',
      },
    },

    Login: {
      screen: Login,
      navigationOptions: {
        title: 'Login',
      },
    },

    SignUp: {
      screen: SignUp,
      navigationOptions: {
        title: 'Cadastro',
      },
    },

    EditQuestion: {
      screen: EditQuestion,
      navigationOptions: {
        title: 'Editar pergunta',
      },
    },

    Question: {
      screen: Question,
      navigationOptions: {
        title: 'Como posso te ajudar?',
      },
    },

    DraftIssues: {
      screen: DraftIssues,
      navigationOptions: {
        title: 'Rascunhos',
      },
    },

    CanceledIssues: {
      screen: CanceledIssues,
      navigationOptions: {
        title: 'Devolvidas/Canceladas',
      },
    },

    AnsweredIssues: {
      screen: AnsweredIssues,
      navigationOptions: {
        title: 'Respondidas',
      },
    },

    SubmittedIssues: {
      screen: SubmittedIssues,
      navigationOptions: {
        title: 'Enviadas',
      },
    },

    Overlay: {
      screen: Overlay,
      navigationOptions: {
        title: 'Respondidas',
      },
    },

    ShowObservation: {
      screen: ShowObservation,
      navigationOptions: {
        title: 'Observação',
      },
    },

    RelatedIssueView: {
      screen: RelatedIssueView,
      navigationOptions: {
        title: 'Pergunta relacionada',
      },
    },

    ShowDetails: {
      screen: ShowDetails,
      navigationOptions: {
        title: 'Detalhes',
      },
    },

    FAQ: {
      screen: FAQ,
      navigationOptions: {
        title: 'Dúvidas frequentes',
      },
    },

    FaqElement: {
      screen: FaqElement,
      navigationOptions: {
        title: 'Dúvida',
      },
    },

    ForwardQuestion: {
      screen: ForwardQuestion,
      navigationOptions: {
        title: 'Encaminhar Paciente',
      },
    },

    RecoverPassword: {
      screen: RecoverPassword,
      navigationOptions: {
        title: 'Recuperar senha',
      },
    },

    Notifications: {
      screen: Notifications,
      navigationOptions: {
        title: 'Notificações',
      },
    },

    Configurations: {
      screen: Configurations,
      navigationOptions: {
        title: 'Configurações',
      },
    },

    Success: {
      screen: Success,
    },
  },
  {
    defaultNavigationOptions: {
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#3c8dbc',
      },
    },
    navigationOptions: {
      tabBarLabel: 'Home!',
    },
    transitionConfig: NavigationConfig,
  },
);

export default createAppContainer(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
