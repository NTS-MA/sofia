package com.huufmasofia1.app;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

public class NotificationService extends Service {

    private static final String ACTION_START_SERVICE = "START";
    public static boolean isServiceRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        startServiceWithNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        startServiceWithNotification();

        return START_STICKY;
    }

    // In case the service is deleted or crashes some how
    @Override
    public void onDestroy() {
        isServiceRunning = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void startServiceWithNotification() {
        Intent service = new Intent(getApplicationContext(), MyTaskService.class);
        Bundle bundle = new Bundle();
        bundle.putString("foo", "bar");
        service.putExtras(bundle);
        getApplicationContext().startService(service);
    }

    void stopMyService() {
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }
}