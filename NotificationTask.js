import Pusher from 'pusher-js/react-native';
import {localNotif} from './src/components/NotifService';
import AsyncStorage from '@react-native-community/async-storage';

module.exports = async taskData => {
  const token = await AsyncStorage.getItem('token');
  const user_id = await AsyncStorage.getItem('id');

  var pusher = new Pusher('8e0be3d3fb4ea49f8cc4', {
    cluster: 'us2',
    authEndpoint: 'http://sofia.huufma.br/api/notification/auth',
    forceTLS: true,
    auth: {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    },
  });

  var channel = pusher.subscribe('private-receivedanswer.' + user_id);
  channel.bind('App\\Events\\ReceivedAnswer', function(data) {
    localNotif(data.message.solicitation_id, data.message.status);
  });
};
